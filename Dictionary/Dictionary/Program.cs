﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            var hayastan = new Country("Armenia");

            var erevan = new City("Erevan");
            var echmiadzin = new City("Echmiadzin");
            var abovyan = new City("Abovyan");

            var tumanyan = new Street("Tumanyan");
            var teryan = new Street("Teryan");
            var saryan = new Street("Saryan");

            var one = new Building("first building");
            var two = new Building("two building");
            var three = new Building("three building");

            hayastan.Add(erevan);
            hayastan.Add(echmiadzin);
            hayastan.Add(abovyan);

            erevan.Add(tumanyan);
            erevan.Add(teryan);
            erevan.Add(saryan);

            echmiadzin.Add(tumanyan);

            abovyan.Add(teryan);

            tumanyan.Add(one);
            tumanyan.Add(two);
            tumanyan.Add(three);

            teryan.Add(one);
            teryan.Add(two);
            teryan.Add(three);

            saryan.Add(one);
            saryan.Add(two);
            saryan.Add(three);

            foreach (var item in hayastan)
            {
                Console.WriteLine(item.Value);
                foreach (var i in erevan)
                {
                    Console.WriteLine(i.Value);
                    foreach (var it in tumanyan)
                    {
                        Console.WriteLine(it.Value);
                    }
                }
            }




        }
    }

    class Building
    {
        public string Number { get; set; }

        public Building(string bu)
        {
            this.Number = bu;
        }

        public override string ToString()
        {
            return Number ;
        }
    }

    class Street : Dictionary<string, Building>
    {
        public string Name { get; set; }

        public Street(string st)
        {
            this.Name = st;
        }

        public void Add(Building bu)
        {
            if (!ContainsKey(bu.Number))
                base.Add(bu.Number, bu);
        }

        public override string ToString()
        {
            return "\n Street -" + Name;
        }
    }

    class City : Dictionary<string, Street>
    {
        public string Name { get; set; }

        public City(string ci)
        {
            this.Name = ci;
        }

        public void Add(Street st)
        {
            if (!ContainsKey(st.Name))
                base.Add(st.Name, st);
        }

        public override string ToString()
        {
            return "\n City -" + Name;
        }
    }

    class Country : Dictionary<string, City>
    {
        private string Name { get; set; }

        public Country(string co)
        {
            this.Name = co;
        }

        public void Add(City ci)
        {
            if (!ContainsKey(ci.Name))
                base.Add(ci.Name, ci);
        }

        public override string ToString()
        {
            return "Country" + Name;
        }
    }
}
